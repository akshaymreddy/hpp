from flask import Flask, render_template, request
import requests
import datetime
import hashlib
import os
from urllib.parse import urlparse
from forms import HiddenFieldForm, UserScheduleForm, Search, ParameterPrecedence
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:root@localhost:8889/hpp'
db = SQLAlchemy(app)

#Database
class UserSchedule(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    location = db.Column(db.String(120), unique=False, nullable=False)
    created_date = db.Column(db.DateTime, default=datetime.datetime.now)

    def __repr__(self):
        return '<User %r>' % self.username

class attacker(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    ip = db.Column(db.String(80))
    created_date = db.Column(db.DateTime, default=datetime.datetime.now)

    def __repr__(self):
        return '<User %r>' % self.ip

class students(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(80))
    email = db.Column(db.String(80))
    mobile = db.Column(db.String(80))

    def __repr__(self):
        return "Name: %r \t Email: %r \t Mobile: %r" % (self.name, self.email, self.mobile)


@app.route('/', methods = ['GET'])
def home():
    return render_template('home.html')

@app.route('/yahoo', methods = ['GET'])
def yahoo():
    return render_template('yahoo.html')

@app.route('/studentlist', methods = ['GET'])
def studentlist():
    # import ipdb; ipdb.set_trace()
    query = "branch=" + request.args.get('branch')
    result = students.query.all()
    return render_template('studentlist.html', result = result, query = query)

@app.route('/studentlist/<pk>', methods = ['GET'])
def action(pk):
    message = ""
    message2 = ""
    if request.args.get('action') == 'view':
        message =  students.query.filter_by(id = pk).all()
    if request.args.get('action') == 'delete':
        obj = students.query.get(pk)
        db.session.delete(obj)
        db.session.commit()
        message2 = "deleted"
    return render_template('studentdetails.html', message = message, message2 = message2)


@app.route('/parameterprecedence', methods = ['GET'])
def parameterprecedence():
    form = ParameterPrecedence(request.form)
    if request.args.get('location'):
        return render_template('parameterprecedence.html', form = form, message = request.args.get('location'))
    return render_template('parameterprecedence.html', form = form)

@app.route('/clienthpp', methods =['GET', 'POST'])
def poll():
    form = ParameterPrecedence(request.form)
    if request.method == 'POST':
        print(request.args)
    return render_template('polls.html', form = form)

@app.route('/voting', methods =['GET','POST'])
def voting():
    poll_id = request.args.get('poll_id','1')
    return render_template('voting.html', poll_id = poll_id)

@app.route('/selectcandidate', methods =['GET', 'POST'])
def selectcandidate():
    import ipdb; ipdb.set_trace()
    poll = request.args.get('poll_id')
    candidate = request.args.get('candidate')
    return render_template('selectcandidate.html', message = [poll, candidate])


@app.route('/honeypot1', methods = ['GET'])
def honeypot():
    form = HiddenFieldForm(request.form)
    message = ""
    if request.args.get('Time_Stamp'):
        message = "Error: Hidden Field is accessed"
    elif request.args.get('First_Name'):
        message = "Form is valid"
    return render_template('HiddenField.html', message = message, form = form)


@app.route('/honeypot2', methods = ['GET','POST'])
def honeypot2():
    register = UserScheduleForm(request.form)  # This form is used to register the Users
    search = Search(request.form) # Display the users based on location
    message = ""
    result = ""
    if request.method == 'POST':
        concat = str(request.form['User_Name']) + str(request.form['Email']) + str(request.form['Location'].lower())
        hash = hashlib.md5(concat.encode('utf-8')).hexdigest()
        if hash == str(request.form['Hash']):
            obj = UserSchedule(username = request.form['User_Name'], email = request.form['Email'], location = request.form['Location'].lower())
            db.session.add(obj)
            db.session.commit()
            message = "You are registered"
        else:
            message = "Input value modified"
            attack = attacker(ip = requests.get('http://ipinfo.io').json()['ip'])
            db.session.add(obj)
            db.session.commit()
    elif request.args.get('SearchKeyword'):
        result = UserSchedule.query.filter_by(location=request.args.get('SearchKeyword').lower()).all()
    return render_template('honeypot2.html', message = message, register = register, search = search, result = result)


@app.route('/serverhpp', methods = ['GET','POST'])
def serverhpp():
    register = UserScheduleForm(request.form)  # This form is used to register the Users
    search = Search(request.form) # Display the users based on location
    message = ""
    result = ""
    if request.method == 'POST':
        obj = UserSchedule(username = request.form['User_Name'], email = request.form['Email'], location = request.form['Location'].lower())
        db.session.add(obj)
        db.session.commit()
        message = "You are registered"
    elif request.args.get('SearchKeyword'):
        result = UserSchedule.query.filter_by(location=request.args.get('SearchKeyword').lower()).all()
    return render_template('UserScheduleForm.html', message = message, register = register, search = search, result = result)



if __name__ == '__main__':
    # ssl_context=('/vagrant/src/HPPAttacks/server.crt','/vagrant/src/HPPAttacks/server.key')
    # ssl_context='adhoc'
    app.run(debug = True, host = 'localhost', port = 8000)
