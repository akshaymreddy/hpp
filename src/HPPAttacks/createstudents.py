from app import db, students

db.drop_all()
db.create_all()

obj1 = students(id=1,name='james',email='james@iu.edu',mobile='784565496654')
obj2 = students(id=2,name='jake',email='jake@iu.edu',mobile='456455996654')
obj3 = students(id=3,name='tony',email='tony@iu.edu',mobile='784587996654')
obj4 = students(id=4,name='alex',email='alex@iu.edu',mobile='784558796654')
obj5 = students(id=5,name='tina',email='tina@iu.edu',mobile='784554576654')

db.session.add(obj1)
db.session.add(obj2)
db.session.add(obj3)
db.session.add(obj4)
db.session.add(obj5)
db.session.commit()
