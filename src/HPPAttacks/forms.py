from wtforms import Form, StringField, HiddenField
from wtforms.fields.html5 import EmailField


class ParameterPrecedence(Form):
    location = StringField()

class polls():
    poll_id = StringField()

class UserRegistration(Form):
    First_Name = StringField(render_kw={"placeholder": "First Name"})
    Last_Name = StringField(render_kw={"placeholder": "Last Name"})

class HiddenFieldForm(Form):
    First_Name = StringField(render_kw={"placeholder": "First Name"})
    Last_Name = StringField(render_kw={"placeholder": "Last Name"})
    Time_Stamp = HiddenField()

class UserScheduleForm(Form):
    User_Name = StringField(render_kw={"placeholder": "username"})
    Email = EmailField(render_kw={"placeholder": "Email"})
    Location = StringField(render_kw={"placeholder": "Location"})
    Hash = StringField()

class Search(Form):
    SearchKeyword = StringField(render_kw={"placeholder": "Enter the location"})
